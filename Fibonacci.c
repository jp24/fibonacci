//fibonacci with error handling

#include <stdio.h>

int fib(int n)
{
    // Zakladamy ze n jest na 100% nieujemne (>= 0)
    int a = 0;
    int b = 1;
    int c = 0;

    for(int i = 0; i < n; i++) {
        c = a + b;
        a = b;
        b = c;
    }
    return a;
}

int call_fib(int n, int* result)
{
    if (n >= 0) {
        *result = fib(n);
        return 1;
    }
    else {
        return 0;
    }
}

int main()
{
    int n;
    scanf("%d", &n);
    int result = 0;

    if (call_fib(n, &result)) {
        printf("Fib(%d)= %d\n", n, result);
    }
    else {
        printf("ERROR ARGUMENT\n");
    }
    return 0;
}
